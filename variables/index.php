<?php

//variables

/*
$nombre = "Juan";
$name = &$nombre;

$nombre = "David";

echo $nombre;
echo $name;
*/

//Variable global

/*
$edad = 41;

function prueba()
{
    global $edad;
    $edad = 22;
}

prueba();

echo $edad;
*/

/*
$altura = 141;

function prueba()
{
    $GLOBALS['altura'] = 172;
}

prueba();

echo $altura;
*/

//Variable estatica

/*
function prueba()
{
    static $x = 0;
    echo $x;
    $x++;
}

prueba();
prueba();
prueba();
*/

//Variable constante

/*
const ESTUDIANTE = "Juan David Barrera Urrego";
echo ESTUDIANTE;
*/

/*
define("PROFESOR", "Diego Alejandro Palacio Valencia");
echo PROFESOR;
*/

//Variables concatenadas

/*
$nombre = "Juan David Barrera Urrego";
$correo = "jdbu.games@gmail.com";

echo $nombre.$correo;
echo $nombre." ".$correo;
echo "Nombre: ".$nombre." Correo: ".$correo;
*/

/*
$numero1 = 1;
$numero2 = 2;
$numero3 = 3;

echo $numero1.$numero2.$numero3;
echo $numero1.$numero3.$numero2;
echo $numero2.$numero1.$numero3;
echo $numero2.$numero3.$numero1;
echo $numero3.$numero1.$numero2;
echo $numero3.$numero2.$numero1;
*/

/*
$numero1 = 1;
$numero2 = 2;
$numero3 = 3;

echo $numero1.$numero2.$numero3." - ";
echo $numero1.$numero3.$numero2." - ";
echo $numero2.$numero1.$numero3." - ";
echo $numero2.$numero3.$numero1." - ";
echo $numero3.$numero1.$numero2." - ";
echo $numero3.$numero2.$numero1;
*/

/*
$numero1 = 1;
$numero2 = 2;
$numero3 = 3;

echo $numero1.$numero2.$numero3."<br>";
echo $numero1.$numero3.$numero2."<br>";
echo $numero2.$numero1.$numero3."<br>";
echo $numero2.$numero3.$numero1."<br>";
echo $numero3.$numero1.$numero2."<br>";
echo $numero3.$numero2.$numero1;
*/

//Prueba variables

    //problema:
/*
funcionPrueba();

altura = 1.72;
$x = 7;
$nombre = Diego;
$_GLOBALS['x'] = 8;
$pais_persona = "Colombia";
$pais = "Mi pais es"$pais_persona;
$valor = $this;
echo "hola"
$coreo;
static $y = .04;
$ciudad "medellin";

function funcionPrueba()
{
    eco "esta es la funcionPrueba";
}
*/

    //Solucion:

    function funcionPrueba()
    {
        echo "esta es la funcionPrueba";
    }

    funcionPrueba();

    $altura = 1.72;
    $x = 7;
    $nombre = "Diego";
    $GLOBALS['x'] = 8;
    $pais_persona = "Colombia";
    $pais = "Mi pais es".$pais_persona;
    $valor = 4542;
    echo "hola";
    $coreo = "jdbu.games@gmail.com";
    static $y = 0.04;
    $ciudad = "medellin";

?>