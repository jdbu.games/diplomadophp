<?php
/*
Dentro de un archivo nuevo, crea las siguientes variables según tu 
criterio:
- Una variable que me permita almacenar el nombre de una persona.
- Una variable “x” para almacenar un número con 2 decimales.
- Una variable constante para almacenar el valor del dólar en pesos 
colombianos.
- Una variable para almacenar los primeros 7 decimales de “pi”.
- Un variable para almacenar los primeros 15 decimales de “Euler”.
- Mostrar el contenido de todas las variables haciendo uso de 
var_dump.
*/

$nombre = "Juan";
$x = 1.05;
const DOLAR = 3763;
$pi = 3.1415926;
$e = 2.718281828459045;
var_dump($nombre);
var_dump($x);
var_dump(DOLAR);
var_dump($pi);
var_dump($e);


?>