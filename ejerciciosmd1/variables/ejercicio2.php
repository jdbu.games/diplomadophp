<?php

/*
Corregir el siguiente ejercicio:

constante PI;
nombre = 'Diego';
$apellido;
estatic $lenguaje = 'PHP';
$saludo 'Hola'
$edad = 18.5 años;
$preciooCamisa = 48.500;
DEFINE('dosVARIABLES', "Hola", "Saludos");
definir "número","9";
global $edad;
$ciudad-1 = "Medellin";
$ApellidoCompleto = "Palacio Valencia";
const Version = 4.2.3;
echo $apellido;
$this = "Colombia";
$x, $y;
const Version = 4.2.4;
$GLOBALES["AÑO"] = 2019;
$suma = x + y;
$ciudad = &nombre;
*/

const PI = 3.1415926;
$nombre = 'Diego';
$apellido = "Barrera";
static $lenguaje = 'PHP';
$saludo = 'Hola';
$edad = "18.5 años";
$preciooCamisa = 48500;
define('dosVARIABLES', "Hola", "Saludos");
define("número","9");
$GLOBALS['edad'] = 27;
$ciudad_1 = "Medellin";
$apellidoCompleto = "Palacio Valencia";
const VERSION = 4.23;
echo $apellido;
$this;
$x;
$y;
const VERSION2 = 4.24;
$GLOBALES["AÑO"] = 2019;
$suma = $x + $y;
$ciudad = "Medellin";


?>