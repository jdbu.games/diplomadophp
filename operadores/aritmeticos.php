<?php

//Operadores aritmeticos

    //Cambio de signo:

/*
echo -(-5);
*/

/*
$x = - 5;

echo -$x;
*/

    //Operador de Suma:

/*
echo 5 + 7;
*/

/*
$numero1 = 7;
$numero2 = 5;

echo $numero1 + $numero2;
*/

    //Operador de resta:

/*
echo 84 - 47;
*/

/*
$numero1 = 84;
$numero2 = 47;

echo $numero1 - $numero2;
*/

    //Operador de multiplicación

/*
echo 5 * 9;
*/

/*
numero1 = 9;
numero2 = 5;

echo $numero1 * $numero2;
*/

    //Operador de división

/*
echo 18 / 4;
*/

/*
$numero1 = 18;
$numero2 = 4;

echo $numero1 / $numero2;
*/

    //Operador de Módulo

/* 
echo 9 % 4;
*/

/*
$numero1 = 9;
$numero2 = 4;

echo $numero1 % $numero2;
*/

    //Operador Exponencial

/*
echo 3 ** 5;
*/

/*
$numero1 = 3;
$numero2 = 5;

echo $numero1 ** $numero2;
*/

//Operadores de Asignación

    //Asignación Simple

/*
$x = 78;

$y = 'David';

$z = true;
*/

/*
$x = 'Hola';

$y = $x;

echo $y;
*/

    //Asignación por Adición

/*
$x = 78;

$x += 7;

echo $x;
*/

/*
$a = 78;
$b = 7;

$b += $a;

echo $b;
*/

    //Asignación por Sustracción

/*
$a = 12;
$a = 7;

$a -= $b;

echo $a;
*/

/*
$a = 12;
$a -= 7;

echo $a;
*/

    //Asignación por Multiplicación
/*
$a = 3;
$b = 4;

$a *= $b;

echo $a;
*/

    //Asignación por División

/*
$a = 9;
$b = 3;

$a /= $b;

echo $a;
*/

/*
$a = 9;
$a /= 3;

echo $a;
*/

    //Asignación por Módulo

/*
$a = 10;
$b = 3;

$a %= $b;

echo $a;
*/

/*
$a = 10;
$b %= 3;

echo $a;
*/

//Operadores de Comparación

    //Operador de Igualdad
        //Caso Verdadero:

/*
$numero1 = 7941;
$numero2 = 7941;

$resultado =  $numero1 == $numero2;

var_dump($resultado);
*/

        //Caso Falso:

/*
$a = 79241 == 7941

var_dump($a);
*/

    //Operador de Identidad
        //Caso falso:

/*
$numero1 = '297';
$numero2 = 297;
$resultado = $numero1 === $numero2;

var_dump($resultado);
*/

        //Caso verdadero

/*
$numero1 = '297';
$numero2 = '297';
$resultado = $numero1 === $numero2;
var_dump($resultado);
*/

    //Operadores de diferencia
        //Caso verdadero Operador <>

/*
$numero1 = 24;
$numero2 = 1990;
$resultado = $numero1 <> $numero2;
var_dump($resultado);
*/

        //Caso Falso Operador <>

/*
$numero1 = 2458;
$numero2 = 2458;
$resultado = $numero1 <> $numero2;
var_dump($resultado);
*/

        //Caso Verdadero Operador !=

/*
$numero1 = 623;
$numero2 = 163;
$resultado = $numero1 != $numero2;
var_dump($resultado);
*/

        //Caso falso operador !=

/*
$numero1 = 8652;
$numero2 = 8652;
$resultado = $numero1 != $numero2;
var_dump($resultado);
*/

    //Operador no Idéntico
        //Caso verdadero

/*
$numero1 = '8652';
$numero2 = 8652;
$resultado = $numero1 !== $numero2;
var_dump($resultado);
*/

        //Caso falso

/*
$numero1 = 58;
$numero2 = 58;
$resultado = $numero1 !== $numero2;
var_dump($resultado);
*/

    //Operador Mayor qué
        //Caso verdadero

/*
$numero1 = 18;
$numero2 = 12;
$resultado = $numero1 > $numero2;
var_dump($resultado);
*/

        //Caso falso

/*
$numero1 = 1248;
$numero2 = 11242;
$resultado = $numero1 > $numero2;
var_dump($resultado);
*/

    //Operador Menor qué
        //Caso verdadero

/*
$numero1 = 25;
$numero2 = 28;
$resultado = $numero1 < $numero2;
var_dump($resultado);
*/

        //Caso falso

/*
$numero1 = 4124;
$numero2 = 228;
$resultado = $numero1 < $numero2;
var_dump($resultado);
*/

    //Operador Mayor o igual qué
        //Caso verdadero

/*
$numero1 = 1387;
$numero2 = 1387;
$resultado = $numero1 >= $numero2;
var_dump($resultado);
*/

        //Caso falso

/*
$numero1 = 985;
$numero2 = 1387;
$resultado = $numero1 >= $numero2;
var_dump($resultado);
*/

    //Operador Menor o igual qué
        //Caso verdadero

/*
$numero1 = 1387;
$numero2 = 1522;
$resultado = $numero1 <= $numero2;
var_dump($resultado);
*/

        //Caso falso

/*
$numero1 = 3641;
$numero2 = 2164;
$resultado = $numero1 <= $numero2;
var_dump($resultado);
*/

//Operadores de incremento y decremento

    //Pre Incremento

/*
$x = 18;

echo ++$x;
*/

    //Pos Incremento

/*
$x = 18;
echo "Primer valor de X = " . $x++;
echo "Segundo valor de X = " . $x;
*/

//Operadores de Decremento

    //Pre Decremento

/*
$x = 18;

echo --$x;
*/

    //Pos Decremento

/*
$x = 18;
echo "Primer valor de X = " . $x--;
echo "Segundo valor de X = " . $x;
*/

//Operadores con cadenas

    //Operador de Concatenación

/*
$texto1 = 'Hola a todos';
echo $texto1. ' ¿cómo están?';
*/

/*
echo 'Hola a todos' . ' ¿cómo están?';
*/

//Operador de Concatenación y Asignación

/*
$texto1 = 'Mi nombre es';
$texto2 = 'Diego Palacio';

$texto1 .= $texto2;

echo $texto2;
*/

//Operador Ternario

/*
$numero = 85;
$resultado = $numero > 50 ? "El número es mayor que 50" : "El número no es mayor que 50";
echo $resultado;
*/

/*
$resultado = "Evaluación de condición" ? "Resultado Verdadero" : "Resultado Falso";

echo $resultado;
*/

/*
$resultado = true ? "Verdadero" : "Falso";
echo $resultado;
*/

/*
$nombrePersona = "Diego";
$resultado = $nombrePersona == "Juan" ? "Es Juan" : "No es Juan";
echo $resultado;
*/

/*
$n1 = 14;
$n2 = 9;
$resultado = $n1 > $n2 ? "Multiplicación de ambos números " . $n1*$n2 : "Error";
echo $resultado;
*/

?>