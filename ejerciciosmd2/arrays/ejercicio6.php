<?php

$pesos = array(80.5, 92.2, 76.4, 105.7, 58.3);

$pesoTotal = 0;
$promedioPeso = 0;
$personasPromedioSup = 0;
$personasPromedioInf = 0;

for ($i=0; $i < 5; $i++) 
{ 
    $pesoTotal += $pesos[$i];
}

$promedioPeso = $pesoTotal / 5;

for ($i=0; $i < 5; $i++) 
{ 
    if($pesos[$i] > $promedioPeso)
    {
        $personasPromedioSup++;
    }
    else if($pesos[$i] < $promedioPeso)
    {
        $personasPromedioInf++;
    }
}

echo "Promedio Peso: " . $promedioPeso . "<br>";
echo "Cantidad de personas que superan el promedio: " . $personasPromedioSup . "<br>";
echo "Cantidad de personas que no superan el promedio: " . $personasPromedioInf . "<br>"


?>