<?php

$notasGrupoA = array(4.5, 5, 3.5, 2.9, 4.2, 3, 2.5, 4.8, 5, 3.8);
$notasGrupoB = array(2.8, 4, 3.5, 4.8, 5, 4.8, 2, 3.1, 1, 5);

$promedioGeneralGrupoA = 0;
$promedioGeneralGrupoB = 0;
$promedioGeneralAmbos = 0;
$grupoMejorPromedio = "";

$sumaTotalNotasA = 0;
$sumaTotalNotasB = 0;


for ($i=0; $i < 10; $i++) 
{ 
    $sumaTotalNotasA += $notasGrupoA[$i];
    $sumaTotalNotasB += $notasGrupoB[$i];
}

$promedioGeneralGrupoA = $sumaTotalNotasA / 10;
$promedioGeneralGrupoB = $sumaTotalNotasB / 10;

$promedioGeneralAmbos = ($promedioGeneralGrupoA + $promedioGeneralGrupoB) / 2;

if($promedioGeneralGrupoA > $promedioGeneralGrupoB)
{
    $grupoMejorPromedio = "Grupo A";
}
else if($promedioGeneralGrupoA < $promedioGeneralGrupoB)
{
    $grupoMejorPromedio = "Grupo B";
}
else
{
    $grupoMejorPromedio = "EMPATE";
}

echo "Promedio General Grupo A: " . $promedioGeneralGrupoA . "<br>";
echo "Promedio General Grupo B: " . $promedioGeneralGrupoB . "<br>";
echo "Promedio General De Ambnos Grupos: " . $promedioGeneralAmbos . "<br>";
echo "El grupo con mejor promedio es el: " . $grupoMejorPromedio . "<br>";

?>