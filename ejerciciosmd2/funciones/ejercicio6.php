<?php

function definirMes($mes)
{
    switch ($mes) {
        case 1:
             return $mes = "Enero";
            break;

        case 2:
             return $mes = "Febrero";
            break;

        case 3:
            return $mes = "Marzo";
            break;

        case 4:
            return $mes = "Abril";
            break;

        case 5:
            return $mes = "Mayo";
            break;

        case 6:
            return $mes = "Junio";
            break;

        case 7:
            return $mes = "Julio";
            break;

        case 8:
            return $mes = "Agosto";
            break;

        case 9:
            return $mes = "Septiembre";
            break;

        case 10:
            return $mes = "Octubre";
            break;

        case 11:
            return $mes = "Noviembre";
            break;

        case 12:
            return $mes = "Diciembre";
            break;

        default:
            return "FUERA DEL RANGO DE MESES";
            break;
    }
}

$nombreMes = definirMes(4);

echo "El mes seleccionado es: " . $nombreMes;

?>