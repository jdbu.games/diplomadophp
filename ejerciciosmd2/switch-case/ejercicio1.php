<?php

function determinarTipoFluido($fluido)
{
    switch ($fluido) {
        case 0:
            echo "No hay establecido un valor definido para el tipo";
            break;
        
        case 1:
            echo "Tipo de fluido: Agua";
            break;

        case 2:
            echo "Tipo de fluido: Gasolina";
            break;

        case 3:
            echo "Tipo de fluido: Hormigón";
            break;
        
        default:
            echo "No existe un valor válido";
            break;
    }
}

determinarTipoFluido(2);

?>