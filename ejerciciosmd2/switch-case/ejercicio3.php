<?php

function consultarNacionalidad($nacionalidad)
{
    switch ($nacionalidad) {
        case 0:
            echo "El Ciudadano es Colombiano";
            break;

        case 1:
            echo "El Ciudadano es Italiano";
            break;

        case 2:
            echo "El Ciudadano es Argentino";
            break;

        case 3:
            echo "El Ciudadano es Alemán";
            break;
        
        default:
            echo "Nacionalidad no especificada en la lista";
            break;
    }
}

consultarNacionalidad(4);

?>