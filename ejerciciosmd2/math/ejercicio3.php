<?php

function operacionesTrigonometricas($valor)
{
    echo "El Seno de " . $valor . " es: " . sin($valor) . "<br>";
    echo "El coseno de " . $valor . " es: " . cos($valor) . "<br>";
    echo "La tangente de " . $valor . " es: " . tan($valor) . "<br>";
}

operacionesTrigonometricas(7);

?>