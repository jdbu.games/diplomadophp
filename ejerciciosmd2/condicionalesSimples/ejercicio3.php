<?php

$num1 = 3;
$num2 = 4;

if($num1 > $num2)
{
    $potencia = pow($num1, $num2);
    echo "El número $num1 es mayor que el número $num2 y su potencia es: $potencia";
}
else
{
    $potencia = pow($num2, $num1);
    echo "El número $num2 es mayor que el número $num1 y su potencia es: $potencia";
}

?>