<?php

function determinarCantidadCifras($numero)
{
    if($numero >= 0 && $numero <= 9)
    {
        echo "el número " . $numero . " es de una cifra";
    }
    else if($numero >= 10 && $numero <= 99)
    {
        echo "el número " . $numero . " es de dos cifras";
    }
    else if($numero >= 100 && $numero <= 999)
    {
        echo "el número " . $numero . " es de tres cifras";
    }
    else if($numero >= 1000 && $numero <= 9999)
    {
        echo "el número " . $numero . " es de cuatro cifras";
    }
    else
    {
        echo "el número " . $numero . " tiene mas de cuatro cifras";
    }
}

determinarCantidadCifras(327);


?>