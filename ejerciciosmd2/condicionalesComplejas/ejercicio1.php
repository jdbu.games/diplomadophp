<?php

function definirCategoriaEdad($edad)
{
    if($edad >= 0 && $edad <= 5)
    {
        echo "Pertenece a la categoría: Infante";
    }
    else if($edad >= 6 && $edad <= 10)
    {
        echo "Pertenece a la categoría: Niño";
    }
    else if($edad >= 11 && $edad <= 15)
    {
        echo "Pertenece a la categoría: Pre Adolescente";
    }
    else if($edad >= 16 && $edad <= 18)
    {
        echo "Pertenece a la categoría: Adolescente";
    }
    else if($edad >= 19 && $edad <= 25)
    {
        echo "Pertenece a la categoría: Pre Adulto";
    }
    else if($edad >= 26 && $edad <= 40)
    {
        echo "Pertenece a la categoría: Adulto";
    }
    else if($edad >= 41 && $edad <= 55)
    {
        echo "Pertenece a la categoría: Pre Anciano";
    }
    else
    {
        echo "Pertenece a la categoría: Anciano";
    }
}

definirCategoriaEdad(27);

?>