<?php

function determinarNumeroMayor($a, $b, $c)
{
    $numeroMayor = max($a, $b, $c);

    return $numeroMayor;
}

$resultado = determinarNumeroMayor(27, 8, 12);

echo "El número mayor es el : " . $resultado;

?>