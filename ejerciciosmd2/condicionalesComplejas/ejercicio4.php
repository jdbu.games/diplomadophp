<?php

function determinarNivel($cantidadPreguntas, $respuestasCorrectas)
{
    if(($respuestasCorrectas * 100) / $cantidadPreguntas >= 90)
    {
        echo "Nivel Máximo";
    }
    else if(($respuestasCorrectas * 100) / $cantidadPreguntas >= 75 && ($respuestasCorrectas * 100) / $cantidadPreguntas < 90)
    {
        echo "Nivel Medio";
    }
    else if(($respuestasCorrectas * 100) / $cantidadPreguntas >= 50 && ($respuestasCorrectas * 100) / $cantidadPreguntas < 75)
    {
        echo "Nivel Regular";
    }
    else if(($respuestasCorrectas * 100) / $cantidadPreguntas < 50)
    {
        echo "Fuera De Nivel";
    }
    
}

determinarNivel(20, 10);

?>