<?php

$palabra = "solos";
$longitudPalabra = strlen($palabra)-1;
$palabraInvertida = "";
$a = 0;

while ($a <= $longitudPalabra) {
    $palabraInvertida[$a] = $palabra[$longitudPalabra - $a];
    $a++;
}

if($palabra == $palabraInvertida)
{
    echo "La palabra " . $palabra . " es palíndroma";
}
else
{
    echo "La palabra " . $palabra . " no es palíndroma";
}

?>