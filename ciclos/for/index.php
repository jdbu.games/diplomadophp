<?php

//Ciclos - Ciclo for

/*
for ($i=0; $i < 10; $i++) 
{ 
        echo $i;
}
*/

/*
for ($i=0; $i <= 20; $i+=2) 
{ 
        echo $i . " ";
}
*/

/*
for ($i=0; $i <= 20; $i++) 
{ 
    if($i % 2  == 0)
    {
        echo $i . " ";
    }
}
*/

/*
for ($i=20; $i >= 0; $i-=2) 
{ 
        echo $i . " ";
}
*/

/*
$multiplos = 0;

for ($i=1; $i <= 1000; $i++) { 
    if($i % 7 == 0)
    {
        $multiplos++;
    }
}

echo "La cantidad de multiplos de 7 es: " . $multiplos;
*/

/*
for ($i=1; $i <= 10; $i++) 
{ 
    echo "Tabla de multiplicar del " . $i . "<br>";
    for ($x=1; $x <= 10; $x++) 
    { 
        echo $i . " * " . $x . " = " . $i * $x . "<br>";
    }
}
*/

for ($i=1; $i <= 2; $i++) { 
    echo "Ciclo 1 <br>";
    for ($x=1; $x <= 2; $x++) { 
        echo "Ciclo 2 <br>";
    }
}

?>