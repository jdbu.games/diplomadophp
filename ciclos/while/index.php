<?php

// Ciclos - While

/*
$x = true;

while ($x)
{
    echo "Ciclo mientras";
    $x = false;
}
*/

/*
$x = 1;

while ($x <= 10) 
{
    echo $x;
    $x++;
}
*/

/*
$x = 1;

while ($x <= 25) 
{
    if($x % 2 != 0)
    {
        echo $x . " ";
    }
    $x++;
}
*/

/*
$palabra = "JUAN BARRERA";
$i = 0;

while ($i < strlen($palabra)) 
{
    echo $palabra[$i] . " ";
    $i++;
}
*/

/*
$palabra = "JUAN BARRERA";
echo "La palabra " . $palabra . " tienen " . strlen($palabra) . " caracteres";
*/

/*
$palabra = "Juan";
$i = 0;

echo "La palabra " . $palabra . " tiene " . strlen($palabra) .  " caracteres <br>";

while ($i < strlen($palabra)) 
{
    echo $palabra[$i] . " en la posición: " . $i . "<br>";
    $i++;
}
*/

/*
$parada = true;
$numero = 0;

while ($parada) 
{
    $numero = rand(1,50);
    echo $numero . " ";

    if($numero % 10 == 0)
    {
        $parada = false;
    }
}
*/

$n = 0;
$numero = 0;

while ($n <= 10) 
{
    $numero = rand(1,20);
    echo $numero . " ";
    $n++;
}

?>