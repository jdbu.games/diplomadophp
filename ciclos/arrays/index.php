<?php

//Arrays

/*
$arrayUno = array("Diego", "Henry", "Sandra", "Lizzeth", "Alex");

$arrayDos[0] = "Diego";
$arrayDos[1] = "Henry";
$arrayDos[2] = "Sandra";
$arrayDos[3] = "Lizzeth";
$arrayDos[4] = "Alex";

echo $arrayUno[1] . "<br>";
echo $arrayUno[3] . "<br>";
echo $arrayUno[4] . "<br>";
*/

/*
$arrayUno = array("Matematicas", "Fisica", "Ingles", "Estadistica");

echo "El total de elementos del array es: " . count($arrayUno);
*/

/*
$arrayUno = array("Matematicas", "Fisica", "Ingles", "Estadistica");

echo "Usando un ciclo for: <br>";

for ($i=0; $i < count($arrayUno); $i++) { 
    echo $arrayUno[$i] . "<br>";
}
*/

/*
$arrayUno = array("Matematicas", "Fisica", "Ingles", "Estadistica");

echo "Usando un foreach: <br>";

foreach ($arrayUno as $dato) 
{
    echo $dato . "<br>";
}
*/

/*
$personas = array();

if($personas != null)
{
    echo "El array tiene datos <br>";
    foreach ($personas as $persona) {
        echo $persona . "<br>";
    }
}
else
{
    echo "Oppss el array está vacío";
}
*/

/*
$personas = array("Diego", "Henry", "Sandra", "Lizzeth", "Alex");

if($personas != null)
{
    echo "El array tiene datos <br>";
    foreach ($personas as $persona) {
        echo $persona . "<br>";
    }
}
else
{
    echo "Oppss el array está vacío";
}
*/

/*
$fechasNacimiento = array("Diego" => "1999-03-12",
"Andres" => "2000-01-05",
"Juan" => "1995-07-05");

echo $fechasNacimiento["Diego"] . "</br>";
echo $fechasNacimiento["Andres"] . "</br>";
echo $fechasNacimiento["Juan"] . "</br>";
*/

/*
$notas = array("Diego" => 4.5, "Juan" => 5, "Alex" => 4);

echo $notas["Diego"] . "</br>";
echo $notas["Juan"] . "</br>";
echo $notas["Alex"] . "</br>";
*/

/*
$notas = array("Diego" => 4.5, "Juan" => "Colombia", "Alex" => true);

echo $notas["Diego"] . "</br>";
echo $notas["Juan"] . "</br>";
echo $notas["Alex"] . "</br>";
*/

/*
$array = array("Pais" => "Argentina", 5325 => "Colombia", 2522 => "Chile");
var_dump($array);
*/

/*
$array = array("a","b", "letras" => "c", "d");
var_dump($array);
*/

/*
$persona = array("Nombre" => "Diego", "Ciudad" => "Medellin", "Altura" => "172cm");
foreach ($persona as $key => $value) {
    echo $key . " - " . $value . "<br>";
}
*/

/*
$empleados = array
(
    array
    (
        "Nombre" => "Juan Barrera",
        "Edad" => "27",
        "Correo" => "jdbu.games@gmail.com"
    ),

    array
    (
        "Nombre" => "Diana Edwards",
        "Altura" => "185cm",
        "Genero" => "Femenino"
    ),

    array
    (
        "Nombre" => "Alex Stevenson",
        "Genero" => "Sin Identificar"
    )

    );

echo $empleados[0]["Nombre"] . "<br>";
echo $empleados[2]["Genero"] . "<br>";
*/

/*
$platos = array
(
    "Corleone" => array
    (
        "Carnes" => "Res y Pollo",
        "Precio" => array
        (
            "Grande" => "$25.000",
            "Mediano" => "$15.000",
            "Pequeño" => "$10.000"
        ),
    ),
);

echo $platos["Corleone"]["Precio"]["Grande"];
*/

/*
$motos = array
(
    array("Moto" => "BMW S100BRR", "Stock" => "7", "Precio" => "80000000"),
    array("Moto" => "Kawasaki Z650", "Stock" => "20", "Precio" => "22000000"),
    array("Moto" => "Susuki DR650", "Stock" => "17", "Precio" => "25000000"),
    array("Moto" => "Yamaha Tracer 900", "Stock" => "2", "Precio" => "49000000")
);

echo $motos[0]["Moto"] . " - Stock: " . $motos[0]["Stock"] . " - Precio: " . $motos[0]["Precio"] . ".<br>";
echo $motos[1]["Moto"] . " - Stock: " . $motos[1]["Stock"] . " - Precio: " . $motos[1]["Precio"] . ".<br>";
echo $motos[2]["Moto"] . " - Stock: " . $motos[2]["Stock"] . " - Precio: " . $motos[2]["Precio"] . ".<br>";
echo $motos[3]["Moto"] . " - Stock: " . $motos[3]["Stock"] . " - Precio: " . $motos[3]["Precio"] . ".<br>";
*/

/*
$ciudades = array("Medellin", "Bogota");

echo "Antes de insertar <br>";

foreach ($ciudades as $ciudad) {
    echo $ciudad ."<br>";
}

array_push($ciudades, "Barranquilla", "Cali");

echo "Después de insertar <br>";

foreach ($ciudades as $ciudad) {
    echo $ciudad ."<br>";
}
*/

/*
$ciudades = array("Medellin", "Bogota", "Barranquilla");

echo "El último elemento es: " . array_pop($ciudades) . "<br>";

foreach ($ciudades as $ciudad) {
    echo $ciudad ."<br>";
}
*/

/*
$ciudades = array("Medellin", "Bogota", "Barranquilla", "Cali");

echo array_search("Bogota", $ciudades);
*/

$ciudades = array("Medellin", "Bogota", "Barranquilla", "Cali");

sort($ciudades);

echo "Array ordenado <br>";

foreach ($ciudades as $ciudad) {
    echo $ciudad ."<br>";
}


?>