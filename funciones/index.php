<?php

//funciones

//Declaración de la función

/*
function saludo()
{
    echo "¡Hola estudiantes!";
}

saludo();
*/

/*
function enviarMail($desde, $destinatarios, $asunto, $mensaje)
{
    echo "Preparando el envío del correo <br>";
    echo "Desde: " . $desde . " <br>";
    echo "Asunto: " . $asunto . " <br>";
    echo "Mensaje: " . $mensaje . " <br>";

    foreach ($destinatarios as $destinatario) 
    {
        echo "Para: " . $destinatario . "<br>";
    }

    echo "Mensaje enviado con éxito <br>";
    echo "<br>";
}

$destinatarios = array("diego@gmail.com", "juan@gmail.com", "stiven@gmail.com");

//Llamado a la 1ra función

enviarMail("jdbu.games@gmail.com", $destinatarios, "Saludo", "Bienvenidos al módulo 2.");

$destinatarios = array("sara@gmail.com", "lisa@gmail.com", "alex@gmail.com");

//Llamado a la 2da función

enviarMail("coppasst@gmail.com", $destinatarios, "Pago", "Envío Pago.");
*/

/*
function sumarNumeros(int $a, int $b)
{
    echo $a + $b;
}

//Llamado a la función

sumarNumeros(5, "5 days");
*/

/*
declare(strict_types=1);

function sumarNumeros(int $a, int $b)
{
    echo $a + $b;
}

//Llamado a la función

sumarNumeros(5, "5 days");
*/

/*
declare(strict_types=1);

function correo(string $correo = "no-repply@gmail.com")
{
    echo "Su correo es: " . $correo . "<br>";
}

//Llamados a la función
correo("diego@gmail.com");
correo();
correo("stiv@gmail.com");
*/


/*
declare(strict_types=1);

function suma(int $numero1 = 0, int $numero2 = 0)
{
    $suma = $numero1 + $numero2;
    echo "El resultado es: " . $suma . "<br>";
}

//Llamados a la función
suma(8,7);
suma();
suma(5);
*/

/*
declare(strict_types=1);

function multiplicacion(int $x = 1, int $y = 1)
{
    return $resultado = $x * $y;
}

echo "2 * 4 = " . multiplicacion(2,4) . "<br>";
echo "5 * 3 = " . multiplicacion(5,3) . "<br>";
echo "7 * 7 = " . multiplicacion(7,7) . "<br>";
*/

/*
declare(strict_types=1);

function guardarTelefonos(string $numero = "0000000", array $telefonos = array())
{
    array_push($telefonos, $numero);
    return $telefonos;
}

function mostrarTelefonos(array $telefonos)
{
    foreach ($telefonos as $telefono) 
    {
        echo $telefono . "<br>";
    }
}

$telefonos = array();

$telefonos = guardarTelefonos("5284136", $telefonos);
$telefonos = guardarTelefonos("5971236", $telefonos);
$telefonos = guardarTelefonos("3214785", $telefonos);
$telefonos = guardarTelefonos("9632587", $telefonos);
$telefonos = guardarTelefonos("6547899", $telefonos);

mostrarTelefonos($telefonos);
*/

/*
declare(strict_types=1);

function calcularIMC(float $altura = 0.0, float $peso = 0.0)
{
    return $peso / ($altura * $altura);
}

echo calcularIMC(1.72,73.2);
*/

/*
declare(strict_types=1);

function sumarFloats(float $a, float $b) : float
{
    return $a + $b;
}

echo sumarFloats(1.2, 5.2);
*/

declare(strict_types=1);

function sumarFloats(float $a, float $b) : string
{
    $suma = $a + $b;
    return "el resultado de $a + $b = $suma Calculo exacto.";
}

echo sumarFloats(1.2, 5.2);

?>