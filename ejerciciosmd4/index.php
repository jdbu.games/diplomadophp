<?php

$Paises = array("Colombia", "Venezuela", "Ecuador", "Argentina", "Mexico", "Otro");
$Departamentos = array("FUERA DE COLOMBIA", "ANTIOQUIA", "ATLANTICO", "CUNDINAMARCA", "VALLE DEL CAUCA", "OTRO");
$Edades = array("Menor de 18", "18 a 25", "26 a 35", "36 a 45", "Mayor a 45");
$Diplomados = array("Programación en PHP", "Programación en Java", "Bases de Datos en MySQL", "Diseño Digital", "Marketing");
$Referencias = array("Bing", "Educaedu", "Emagister", "Facebook", "Google", "Instagram", "Paso por la institucion", "Radio online/Radio en internet", "Recibio información por correo", "Referido", "Volante", "Yahoo");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Diplomado</title>
</head>
<body>
    <h1>Por favor diligencia toda la información</h1>

    <form method="POST" action="informacionInscripcion.php">
        <input type="number" name="Documento" required="" placeholder="Cédula, CURP o C.I.P. Sin puntos y comas."><br><br>
        <input type="number" name="ConfirmarDocumento" required="" placeholder="Confirmar Cédula, CURP o C.I.P. Sin puntos y comas."><br><br>
        <input type="text" name="Nombres" required="" placeholder="Nombres"><br><br>
        <input type="text" name="Apellidos" required="" placeholder="Apellidos"><br><br>
        <input type="email" name="Correo" required="" placeholder="Correo Electrónico"><br><br>
        <input type="email" name="ConfirmarCorreo" required="" placeholder="Confirmar Correo Electrónico"><br><br>
        <select name="Pais" required="">
            <option>Pais</option>
            <?php
                foreach ($Paises as $Pais) 
                { 
            ?>     
                    <option value="<?php echo $Pais; ?>"><?php echo $Pais; ?></option>
            <?php
                }
            ?>
        </select><br><br>
        <input type="text" name="Departamento" required="" placeholder="Departamento/Estado/Provincia"><br><br>
        <input type="text" name="Municipio" required="" placeholder="Municipio/Ciudad"><br><br>
        <select name="Edad" required="">
            <option>Edad</option>
            <?php
                foreach ($Edades as $Edad) 
                { 
            ?>     
                    <option value="<?php echo $Edad; ?>"><?php echo $Edad; ?></option>
            <?php
                }
            ?>
        </select><br><br>
        <input type="text" name="Direccion" required="" placeholder="Dirección de residencia"><br><br>
        <input type="text" name="Telefono" required="" placeholder="Télefono de contacto"><br><br>
        <select name="Diplomado" required="">
            <option>Diplomado</option>
            <?php
                foreach ($Diplomados as $Diplomado) 
                { 
            ?>     
                    <option value="<?php echo $Diplomado; ?>"><?php echo $Diplomado; ?></option>
            <?php
                }
            ?>
        </select><br><br>
        <select name="Informacion" required="">
            <option>¿Como se enteró?</option>
            <?php
                foreach ($Referencias as $Referencia) 
                { 
            ?>     
                    <option value="<?php echo $Referencia; ?>"><?php echo $Referencia; ?></option>
            <?php
                }
            ?>
        </select><br><br>
        <input type="radio" name="Genero" required="" value="Masculino">Masculino
        <input type="radio" name="Genero" required="" value="Femenino">Femenino<br><br>
        <input type="radio" name="Politica" required="">Por favor acepta los terminos y condiciones<br><br>
        <input type="radio" name="Politica" required="">Lee el habeas data<br><br>
        <input type="submit" value="Registrar">

</body>
</html>