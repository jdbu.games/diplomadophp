<?php

/*
$x = 1;

switch ($x) {
    case 0:
        echo "X es igual a 0";
        break;
    
    case 1:
        echo "X es igual a 1";
        break;

    case 2:
        echo "X es igual a 2";
        break;
}
*/

/*
$fruta = "Mango";

switch ($fruta) {
    case "Manzana":
        echo "La fruta es una manzana";
        break;
    
    case "Mango":
        echo "La fruta es un Mango";
        break;

    case "Pera":
        echo "La fruta es una Pera";
        break;
}
*/

/*
$numero = 1;

switch ($numero) {
    case 1:
        echo "Caso 1 ";
    case 2:
        echo "Caso 2 ";
        break;
    case 3:
        echo "Caso 3 ";
}
*/

/*
$nombre = "Juan";

switch ($nombre) {
    case "Nicolas":
        echo "Hola Nicolas";
        break;
    
    case "Jehison":
        echo "Hola Jehison";
        break;

    case "Pera":
        echo "La fruta es una Pera";
        break;
    
    default:
        echo "Hola sea quien seas";
}
*/

/*
$x = 8;
$y = 4;

$operacion = "/";

switch ($operacion) 
{
    case "+":
        $suma = $x + $y;
        echo "Resultado de la suma: " . $suma;
        break;
    
    case "-":
        $resta = $x - $y;
        echo "Resultado de la resta: " . $resta;
        break;

    case "*":
        $multiplicacion = $x * $y;
        echo "Resultado de la multiplicación: " . $multiplicacion;
        break;

    case "/":
        $division = $x / $y;
        echo "Resultado de la división: " . $division;
        break;
    
    default:
        echo "La operación no es válida";
}
*/

$notaParcial = 4;
$notaFinal = 4;

switch ($notaFinal) 
{
    case 1:
    case 2:
    case 3:
        echo "Perdiste la materia <br> ";
        break;

    case 4:
    case 5:
        echo "Ganaste la materia <br>";
        switch ($notaParcial) 
        {
            case 1:
            case 2:
            case 3:
                echo "Pero perdiste el parcial ";
                break;

            case 4:
            case 5:
                echo "y tambien el parcial";
                break;
            
            default:
                echo "error";
        }
        break;
    
    default:
        echo "error";
        break;
}



?>