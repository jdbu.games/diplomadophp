<?php

require_once('../../Usuarios/Modelo/Usuarios.php');

$ModeloUsuario = new Usuarios();
$ModeloUsuario->validateSession();

$Id = $_GET['Id'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sistema de Notas</title>
</head>
<body>
    <h1>Eliminar Estudiante</h1>
    <form action="../Controladores/delete.php" method="POST">
    <input type="hidden" name="Id" value="<?php echo $Id ?>">
    <p>¿Estás seguro que deseas eliminar este estudiante?</p>
    <input type="submit" value="Eliminar Estudiante">
</form>

</body>
</html>