<?php

require_once('../../Usuarios/Modelo/Usuarios.php');

$ModeloUsuarios = new Usuarios();
$ModeloUsuarios->validateSession();

$Id = $_GET['Id'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sistema de Notas</title>
</head>
<body>
    <h1>Eliminar Materia</h1>
    <form action="../Controladores/delete.php" method="POST">
    <input type="hidden" name="Id" value="<?php echo $Id ?>">
    <p>¿Estás seguro que deseas eliminar esta materia?</p>
    <input type="submit" value="Eliminar Materia">
</form>

</body>
</html>