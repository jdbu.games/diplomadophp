<?php

//Tipos de datos

    //String (cadena de texto)
/*
echo "Hola a todos <br>";
echo 'Cómo están';
*/

    //Consultar tipo de dato
/*
$ciudad = "Medellín";
var_dump($ciudad);
*/

    //Int / integer (Números Enteros)
/*
$numero = 8417;
var_dump($numero);
*/

    //Float / Double (Números Decimales)
/*
$numero_decimal = -7.054;
var_dump($numero_decimal);
*/

    //Boolean (Verdadero o false)
/*
$verdadero = true;
var_dump($verdadero);
$falso = false;
var_dump($falso);
*/

/*
var_dump((bool) "");
var_dump((bool) 1);
var_dump((bool) -2);
var_dump((bool) "foo");
var_dump((bool) 2.3e5);
var_dump((bool) array());
var_dump((bool) "false");
*/

    //Arrays (Arreglos)
/*
$ciudades = array("Medellín", "Bogota", "Cali", "Cartagena");

var_dump($ciudades);
*/

    //Null

$x = "Juan";
var_dump($x);
$x = null;
var_dump($x);

?>
