<?php

//Condicionales

$velocidad = 89;

if($velocidad >= 0 and $velocidad <= 30)
{
    echo "Zonas escolares";
}
else if($velocidad > 30 and $velocidad <= 60)
{
    echo "Vías urbanas";
}
else if($velocidad > 60 and $velocidad <= 80)
{
    echo "Vías rurales";
}
else if($velocidad > 80 and $velocidad <= 100)
{
    echo "Rutas nacionales";
}
else
{
    echo "Estás infringiendo los límites de velocidad";
}

?>