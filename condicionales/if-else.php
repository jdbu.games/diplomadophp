<?php

//Condicionales

/*
if(25 > 6*5)
{
    echo "Excelenta, la condicional es verdadera";
}
else
{
    echo "Oppps, la condicional es falsa";
}
*/

$pregunta1 = "Si";
$pregunta2 = "No";

echo "¿Eres de Colombia? </br>";
echo "Respuesta: " . $pregunta1 . "</br>";

if($pregunta1 == "Si")
{
    echo "¿Eres de Antioquia? </br>";
    echo "Respuesta: " . $pregunta2 . "</br>";

    if($pregunta2 == "Si")
    {
    echo "Bienvenido </br>";
    }
    else
    {
        echo "Vísitanos </br>";
    }
}
else
{
    echo "Eres Extranjero </br>";
}

?>