<?php

//Condicionales

/*
if( 5 > 3 && "Juan" == "Juan")
{
    echo "5 es mayor que 3 y Juan es el nombre correcto" ;
}
*/

/*
$p = true;
$q = true;

//Condicional 1

if ($p && $q) 
{
    echo "P y Q tienen un valor verdadero <br>";
}

//Condicional 2

if($p || $q)
{
    echo "P o Q tienen un valor verdadero <br>";
}

//Condicional 3

if(!$p)
{
    echo "P tiene un valor de true <br>";
}
*/

/*
$p = true;
$q = false;

//Condicional 1

if ($p && $q) 
{
    echo "P y Q tienen un valor verdadero <br>";
}

//Condicional 2

if($p || $q)
{
    echo "P o Q tienen un valor verdadero <br>";
}

//Condicional 3

if(!$p)
{
    echo "P tiene un valor de true <br>";
}
*/

/*
$p = false;
$q = true;

//Condicional 1

if ($p && $q) 
{
    echo "P y Q tienen un valor verdadero <br>";
}

//Condicional 2

if($p || $q)
{
    echo "P o Q tienen un valor verdadero <br>";
}

//Condicional 3

if(!$p)
{
    echo "P tiene un valor de true <br>";
}
*/

$p = false;
$q = false;

//Condicional 1

if ($p && $q) 
{
    echo "P y Q tienen un valor verdadero <br>";
}

//Condicional 2

if($p || $q)
{
    echo "P o Q tienen un valor verdadero <br>";
}

//Condicional 3

if(!$p)
{
    echo "P tiene un valor de true <br>";
}

?>