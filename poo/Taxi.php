<?php

require_once('Vehiculo.php');

class Taxi extends Vehiculo
{
    private $licencia;

    public function __construct(string $licencia = "Nula")
    {
        $this->licencia = $licencia;
    }

    public function __get($propety)
    {
        return property_exists($this, $propety) ? $this->$propety : "No existe";
    }

    public function __set($propety, $value)
    {
        return property_exists($this, $propety) ? $this->$propety = $value : "No existe";
    }
}