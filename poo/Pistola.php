<?php

require_once('Arma.php');

class Pistola implements Arma
{
    private $cantidad;

    public function recargar()
    {
        echo "El arma está cargada <br>";
    }

    public function disparar($cantidad)
    {
        echo "Disparaste " . $cantidad . " tiros <br>";
    }

    public function cambiar($arma)
    {
        echo "Cambiaste tu arma por: " . $arma . "<br>";
    }
}

?>