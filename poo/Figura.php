<?php

abstract class Figura
{
    public function __construct($color)
    {
        $this->color = $color;
    }

    abstract public function calcularArea();

    public function getColor()
    {
        return $this->color;
    }
}

?>