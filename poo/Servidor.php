<?php

class Servidor
{
    private $ip;
    private $version;
    private $usuario;
    private $password;

    public function __construct(string $ip = "172.0.0.1",
        int $version = 1, 
        string $usuario = "root",
        string $password = "123")
    {
        $this->ip = $ip;
        $this->version = $version;
        $this->usuario = $usuario;
        $this->password = $password;
        echo "Servidor iniciado. <br>";
    }

    public function ingresar(string $usuario, string $password) : void {
        if($usuario == $this->usuario AND $password == $this->password)
        {
            echo "Bienvenido administrador. <br>";
        }
        else
        {
            echo "Lo siento, contraseña incorecta. <br>";
        }
    }

    public function __get($propety)
    {
        return property_exists($this, $propety) ? $this->$propety : "No existe";
    }

    public function __set($propety, $value)
    {
        return property_exists($this, $propety) ? $this->$propety = $value : "No existe";
    }

    public function actualizarServidor(int $versiones = 0) : int
    {
        return $this->version += $versiones;
    }

    public function informacionServidor(string $ip) : string
    {
        return "IP: " . $this->ip . " - Version: " . $this->version;
    }
}

?>