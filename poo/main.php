<?php

/*
require_once('gato.php');

$GatoBlanco = new Gato();

$GatoBlanco->pasear();
$GatoBlanco->tomarLeche();
*/

/*
require_once('Calculadora.php');

$Calculadora1 = new Calculadora();

$Calculadora1->suma(5, 9);
$Calculadora1->resta(18, 2);
$Calculadora1->multiplicacion(5, 9);
$Calculadora1->division(5, 9);
*/

/*
require_once('Correo.php');

$CorreoGmail = new Correo();

$CorreoGmail->setDe("Jdbu.games@gmail.com");
echo 'De: ' . $CorreoGmail->getDe() . '<br>';

$CorreoGmail->setAsunto("Invitación - Familiar");
echo 'Asunto: ' . $CorreoGmail->getAsunto() . '<br>';

$CorreoGmail->setMensaje("Hola, los invito a la cena familiar");
echo 'Mensaje: ' . $CorreoGmail->getMensaje() . '<br>';

$CorreoGmail->setPara("papa@gmail.com, mama@gmail.com, hermano@gmail.com");
echo 'Para: ' . $CorreoGmail->getPara() . '<br>';
*/

/*
require_once('Calculadora.php');

$Calculadora1 = new Calculadora(9, 3);

$Calculadora1->suma();
$Calculadora1->resta();
$Calculadora1->multiplicacion();
$Calculadora1->division();
*/

/*
require_once('Palabras.php');

$palabrita = new Palabra("Politecnico");
$palabrita->mostrarPalabra();
*/

/*
require_once('Calculadora.php');

$casio = new Calculadora();

$numeros = array(5, 9, 2, 3, 4, 7);

$casio->sumarArray($numeros);
*/

/*
require_once('Calculadora.php');

$Casio = new Calculadora(54, "-", 12);

$Casio->dibujarOperacion();
*/

/*
require_once('Calculadora.php');

$Casio = new Calculadora();

$Casio->setNumero1(84);
$Casio->setOperador("/");
$Casio->setNumero2(4);

$Casio->dibujarOperacion();
*/

/*
require_once('Destruir.php');

$Destruct = new Destruir();
$Destruct->asignarNombre("Destrucción 1");
$Destruct->mostrarDestruccion();
*/

/*
require_once('Carro.php');

$Mazda3 = new Carro();

$Mazda3->encender();
$Mazda3->subirPasajero();
$Mazda3->subirPasajero();
$Mazda3->acelerar(30);
$Mazda3->frenar(30);
$Mazda3->apagar();
$Mazda3->subirPasajero();
$Mazda3->encender();
$Mazda3->acelerar(40);
$Mazda3->acelerar(99);
$Mazda3->choque();
$Mazda3->frenar(10);
$Mazda3->acelerar(20);
$Mazda3->choque();
$Mazda3->acelerar(20);
$Mazda3->choque();
*/

/*
require_once('Adivina.php');

$Juego = new Adivina(1,5,4);
*/

/*
require_once('Casa.php');

$miCasita = new Casa(85000000);
$precioCasita = $miCasita->aumentarPrecio(20000000);

echo "El precio de la casita es: " . $precioCasita;
*/

require_once('Casa.php');

$miCasita = new Casa(85000000, "Juan Barrera", true);
$precioCasita = $miCasita->aumentarPrecio(20000000);
$miCasita->cambiarPropietario("Vanesa Alcaraz");
$estadoCasita = $miCasita->estadoCasa();

echo "El precio de la casita es: " . number_format($precioCasita) . " y " . $estadoCasita;


?>