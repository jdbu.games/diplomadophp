<?php

require_once('Figura.php');

class Triangulo extends Figura
{
    private $base;
    private $altura;

    public function __construct($color = "blanco", $base = 1, $altura = 1)
    {
        parent::__construct($color);
        $this->base = $base;
        $this->altura = $altura; 
    }

    public function calcularArea()
    {
        return $this->base * $this->altura / 2;
    }
}

?>