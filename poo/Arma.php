<?php

interface Arma
{
    public function recargar();

    public function disparar($cantidad);

    public function cambiar($arma);
}

?>