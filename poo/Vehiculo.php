<?php

class Vehiculo
{
    private $matricula;
    private $modelo;
    private $potencia;

    public function __construct(string $matricula = "", string$modelo = "", string $potencia="")
    {
        $this->matricula = $matricula;
        $this->modelo = $modelo;
        $this->potencia = $potencia;
    }

    public function __get($propety)
    {
        return property_exists($this, $propety) ? $this->$propety : "No existe";
    }

    public function __set($propety, $value)
    {
        return property_exists($this, $propety) ? $this->$propety = $value : "No existe";
    }

    public function encenderVehiculo() : void
    {
        echo "Vehiculo encendido <br>";
    }

    public function apagarVehiculo() : void
    {
        echo "Vehiculo apagado <br>";
    }
}



?>