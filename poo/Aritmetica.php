<?php

class Aritmetica
{
    public function suma(int ...$numeros)
    {
        $suma = 0;
        foreach ($numeros as $numero)
        {   
            $suma += $numero;
        }
        return $suma;
    }

    public function resta(int $numero1, int $numero2) : int
    {
        $resta = $numero1 - $numero2;
        return $resta;
    }

    public function multiplicacion(int ...$numeros) : int
    {
        $multiplicacion = 1;
        foreach ($numeros as $numero) 
        {
            $multiplicacion *= $numero;
        }

        return $multiplicacion;
    }

    public function division(int $numero1, int $numero2) : float
    {
        $division = $numero1 / $numero2;
        return $division;
    }
}

?>