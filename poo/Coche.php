<?php

interface Coche
{
    public function tanquear();

    public function velocidad($velocidad);

    public function mantenimiento($fecha);
}

?>