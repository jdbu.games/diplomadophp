<?php

/*
class Calculadora
{
    public function suma($numero1, $numero2)
    {
        $suma = $numero1 + $numero2;
        echo "La suma es: " . $suma . "<br>"; 
    }

    public function resta($numero1, $numero2)
    {
        $resta = $numero1 - $numero2;
        echo "La resta es: " . $resta . "<br>"; 
    }

    public function multiplicacion($numero1, $numero2)
    {
        $multiplicacion = $numero1 * $numero2;
        echo "La multiplicación es: " . $multiplicacion . "<br>"; 
    }

    public function division($numero1, $numero2)
    {
        $division = $numero1 / $numero2;
        echo "La división es: " . $division . "<br>"; 
    }
}
*/

/*
class Calculadora
{
    private $numero1;
    private $numero2;

    public function __construct($numero1, $numero2)
    {
        $this->numero1 = $numero1;
        $this->numero2 = $numero2;
    }

    public function suma()
    {
        $suma = $this->numero1 + $this->numero2;
        echo "La suma es: " . $suma . "<br>"; 
    }

    public function resta()
    {
        $resta = $this->numero1 - $this->numero2;
        echo "La resta es: " . $resta . "<br>"; 
    }

    public function multiplicacion()
    {
        $multiplicacion = $this->numero1 * $this->numero2;
        echo "La multiplicación es: " . $multiplicacion . "<br>"; 
    }

    public function division()
    {
        $division = $this->numero1 / $this->numero2;
        echo "La división es: " . $division . "<br>"; 
    }
}
*/

/*
class Calculadora
{
    public function sumarArray($array)
    {
        $resultado = 0;
        for ($i=0; $i < count($array); $i++) { 
            $resultado = $array[$i] + $resultado;
        }
        echo "El resultado es: " . $resultado;
    }
}
*/

/*
class Calculadora
{
    private $numero1;
    private $operador;
    private $numero2;

    public function __construct($numero1, $operador, $numero2)
    {
        $this->numero1 = $numero1;
        $this->operador = $operador;
        $this->numero2 = $numero2;
    }

    public function dibujarOperacion()
    {
        switch ($this->operador) {
            case '+':
                $this->dibujarSuma();
                break;

            case '-':
                $this->dibujarResta();
                break;

            case '/':
                $this->dibujarDivision();
                break;

            case '*':
                $this->dibujarMultiplicacion();
                break;
            
            default:
                echo "Error de operador";
                break;
        }
    }

    public function dibujarSuma()
    {
        $suma = $this->numero1 + $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $suma;
    }

    public function dibujarResta()
    {
        $resta = $this->numero1 - $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $resta;
    }

    public function dibujarDivision()
    {
        $division = $this->numero1 / $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $division;
    }

    public function dibujarMultiplicacion()
    {
        $multiplicacion = $this->numero1 * $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $multiplicacion;
    }
}
*/

/*
class Calculadora
{
    private $numero1;
    private $numero2;

    public function __construct($numero1, $numero2)
    {
        $this->numero1 = $numero1;
        $this->numero2 = $numero2;
    }

    public function suma()
    {
        $suma = $this->numero1 + $this->numero2;
        echo "La suma es: " . $suma . "<br>"; 
    }

    public function resta()
    {
        $resta = $this->numero1 - $this->numero2;
        echo "La resta es: " . $resta . "<br>"; 
    }

    public function multiplicacion()
    {
        $multiplicacion = $this->numero1 * $this->numero2;
        echo "La multiplicación es: " . $multiplicacion . "<br>"; 
    }

    public function division()
    {
        $division = $this->numero1 / $this->numero2;
        echo "La división es: " . $division . "<br>"; 
    }
}
*/

/*
class Calculadora
{
    public function sumarArray($array)
    {
        $resultado = 0;
        for ($i=0; $i < count($array); $i++) { 
            $resultado = $array[$i] + $resultado;
        }
        echo "El resultado es: " . $resultado;
    }
}
*/

class Calculadora
{
    private $numero1;
    private $operador;
    private $numero2;

    public function __construct()
    {
        
    }

    public function setNumero1($numero1)
    {
        $this->numero1 = $numero1;
    }

    public function getNumero1()
    {
        return $this->numero1;
    }

    public function setOperador($operador)
    {
        $this->operador = $operador;
    }

    public function getOperador()
    {
        return $this->operador;
    }

    public function setNumero2($numero2)
    {
        $this->numero2 = $numero2;
    }

    public function getNumero2()
    {
        return $this->numero2;
    }

    public function dibujarOperacion()
    {
        switch ($this->operador) {
            case '+':
                $this->dibujarSuma();
                break;

            case '-':
                $this->dibujarResta();
                break;

            case '/':
                $this->dibujarDivision();
                break;

            case '*':
                $this->dibujarMultiplicacion();
                break;
            
            default:
                echo "Error de operador";
                break;
        }
    }

    public function dibujarSuma()
    {
        $suma = $this->numero1 + $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $suma;
    }

    public function dibujarResta()
    {
        $resta = $this->numero1 - $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $resta;
    }

    public function dibujarDivision()
    {
        $division = $this->numero1 / $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $division;
    }

    public function dibujarMultiplicacion()
    {
        $multiplicacion = $this->numero1 * $this->numero2;
        echo $this->numero1 . $this->operador . $this->numero2 . " = " . $multiplicacion;
    }
}

?>