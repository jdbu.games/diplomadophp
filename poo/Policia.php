<?php

require_once('Arma.php');
require_once('Coche.php');

class Policia implements Arma, Coche
{
    public function recargar()
    {
        echo "Arma cargada <br>";
    }

    public function disparar($cantidad)
    {
        echo "Disparaste " . $cantidad . " tiros <br>";
    }

    public function cambiar($arma)
    {
        echo "Cambiaste tu arma por: " . $arma . "<br>";
    }

    public function tanquear()
    {
        echo "Deposito de gasolina lleno <br>";
    }

    public function velocidad($velocidad)
    {
        echo "La velocidad del coche es: " . $velocidad . "<br>";
    }

    public function mantenimiento($fecha)
    {
        echo "Debes realizar el mantenimiento la siguiente fecha: " . $fecha . "<br>";
    }
}

?>