<?php

require_once('Vehiculo.php');

class Autobus extends Vehiculo
{
    private $puestos;

    public function encenderVehiculo() : void 
    {
        echo "Autobus encendido <br>";
    }

    public function __construct(int $puestos = 1)
    {
        $this->puestos = $puestos;
    }

    public function __get($propety)
    {
        return property_exists($this, $propety) ? $this->$propety : "No existe";
    }

    public function __set($propety, $value)
    {
        return property_exists($this, $propety) ? $this->$propety = $value : "No existe";
    }
}