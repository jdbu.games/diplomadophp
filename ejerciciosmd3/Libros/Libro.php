<?php

class Libro
{
    private $autor;
    private $titulo;
    private $paginas;

    public function __construct()
    {

    }

    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    public function getAutor()
    {
        return $this->autor;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setPaginas($paginas)
    {
        $this->paginas = $paginas;
    }

    public function getPaginas()
    {
        return $this->paginas;
    }

    public function mostrarLibro()
    {
        echo "El libro " . $this->titulo . ", del autor " . $this->autor .
        ", tiene " . $this->paginas . " páginas <br>";
    }

    public function compararLibros($titulo1, $numPaginas1, $titulo2, $numPaginas2)
    {
        $resultado = max($numPaginas1, $numPaginas2);
        if($resultado == $numPaginas1)
        {
            echo "El libro " . $titulo1 . " tiene más páginas. <br>";
        }
        else if($resultado == $numPaginas2)
        {
            echo "El libro " . $titulo2 . " tiene más páginas. <br>";
        }
        else
        {
            echo "Ambos libros tienen el mismo número de páginas.";
        }
    }
}

?>